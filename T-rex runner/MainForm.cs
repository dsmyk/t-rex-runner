﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;

namespace T_rex_runner
{
	public partial class MainForm : Form
	{
		bool jumping = false;
		int jumpSpeed = 10;
		int force = 15;
		int score = 0;
		int runningSpeed = 9;
		
		int nextSpeedUpAt = 0;
		
		int scoreCounter = 5;
		
		int easterEggCounter = 0;
		int easterEggTimeout = 30;
		
		Control randomObstacle;
		Control currentObstacle;
		
		List<Control> obstacles;
		
		Random random = new Random();
		
		public MainForm()
		{
			InitializeComponent();
			CreateListOfObstacles();
			int randomIndex = random.Next(obstacles.Count);
			randomObstacle = obstacles[randomIndex];
			currentObstacle = randomObstacle;
		}
		
		private void Logs()
		{
			Console.Write("Jumping: " + jumping);
			Console.Write("   Jumping speed: " + jumpSpeed);
			Console.WriteLine("   Force: " + force);
			Console.Write("Runing speed: " + runningSpeed);
			Console.WriteLine("   Next speedup at: " + nextSpeedUpAt);
			for(int i = 0; i < obstacles.Count; i++)
			{
				Console.Write("Index:" + i);
				Console.Write(" X:" + obstacles[i].Left + " / ");
			}
			Console.WriteLine("");
			Console.WriteLine("----------------------------------------------------------------------------------------------------");
		}
		
		private void JumpPhisics()
		{
			if(jumping && force < 5)
			{
				jumping = false;
			}
			if(jumping)
			{
				jumpSpeed = -20;
				force -= 1;
			}
			else
			{
				jumpSpeed = 10;
			}
			
			if(player.Top >= 365 && !jumping)
			{
				force = 15;
				player.Top = 375;
				jumpSpeed = 0;
			}
		}
		
		private void CreateListOfObstacles()
		{
			obstacles  = new List<Control>();
			
			foreach(Control x in this.Controls)
			{
				if(x is PictureBox && (string)x.Tag == "obstacle")
				{
					obstacles.Add(x);
				}
			}
		}
		
		private void SpawnAndMoveObstacles()
		{
			
			int randomIndex = random.Next(obstacles.Count);
			if(obstacles[randomIndex].Right >= 0)
			{
				randomIndex = random.Next(obstacles.Count);
			}
			else if(obstacles[randomIndex].Right <= 0 && currentObstacle.Right <= 700)
			{
				randomObstacle = obstacles[randomIndex];
				currentObstacle = randomObstacle;
				currentObstacle.Left = 1300;


			}
			
			foreach(Control x in this.Controls)
			{
				if(x is PictureBox && (string)x.Tag == "obstacle" && x.Left >= -130)
				{
					x.Left -= runningSpeed;
				}
			}
			
		}
		
		private void ScaleRunningSpeedWithScore()
		{
			if(score >= nextSpeedUpAt)
			{
				runningSpeed++;
				nextSpeedUpAt = score + 100;
			}
		}
		
		
		void GameTimerTick(object sender, EventArgs e)
		{
			Logs();
			if(easterEggCounter < 200)
				easterEggCounter++;
			
			if(easterEggTimeout == 0)
			{
				easterEggCounter = 0;
				easterEggTimeout = 30;
			}
			
			if(easterEggCounter == 200)
			{
				if(easterEggTimeout >= 0)
				{
					Console.WriteLine("");
					Console.WriteLine("Pozdrawiam mame tate i rodzicuf");
					Console.WriteLine("1Tib tez :)");
					Console.WriteLine("XJZD");
					easterEggTimeout--;
				}
			}
			
			player.Top += jumpSpeed;
			
			scoreCounter--;
			if(scoreCounter == 0)
			{
				score++;
				scoreCounter = 5;
			}
			
			JumpPhisics();
			SpawnAndMoveObstacles();
			ScaleRunningSpeedWithScore();
			
			textBox.Text = "Wynik: " + score;
			
			
			foreach(Control x in this.Controls)
			{			
				if(x is PictureBox && (string)x.Tag == "obstacle")
				{
				if(player.Bounds.IntersectsWith(x.Bounds))
				{
					GameTimer.Enabled = false;
					textBox.Text += "       Koniec gry";
				}
				}
			}
		}
		
		private void RestartGame()
		{
			score = 0;
			force = 15;
			player.Top = 370;
			jumpSpeed = 10;
			runningSpeed = 9;
			textBox.Text = "Wynik: " + score;
			
			foreach(Control x in this.Controls)
			{
				if(x is PictureBox && (string)x.Tag == "obstacle")
				{
					x.Left = -130;
				}
			}
			
			GameTimer.Enabled = true;
		}
		
		void MainFormKeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Up && !jumping)
			{
				jumping = true;
			}
			
			if(e.KeyCode == Keys.P)
			{
				GameTimer.Enabled = false;
			}
			
			if(e.KeyCode == Keys.S)
			{
				GameTimer.Enabled = true;
			}

		}
		
		void MainFormKeyUp(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.R)
			{
				RestartGame();
			}
			
			if(e.KeyCode == Keys.Up && jumping)
			{
				jumping = false;
			}
		}
		
		void MainFormLoad(object sender, EventArgs e)
		{
			AllocConsole();
		}
		
		//Allows comand line to be seen during normal run
		[DllImport("kernel32.dll",SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool AllocConsole();
	}
}