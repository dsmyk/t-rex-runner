﻿/*
 * Created by SharpDevelop.
 * User: User
 * Date: 2018-06-06
 * Time: 18:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace T_rex_runner
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.GameTimer = new System.Windows.Forms.Timer(this.components);
			this.ground = new System.Windows.Forms.PictureBox();
			this.malykaktus1 = new System.Windows.Forms.PictureBox();
			this.malykaktus2 = new System.Windows.Forms.PictureBox();
			this.malykaktus3 = new System.Windows.Forms.PictureBox();
			this.duzykaktus1 = new System.Windows.Forms.PictureBox();
			this.duzykaktus2 = new System.Windows.Forms.PictureBox();
			this.duzykaktus3 = new System.Windows.Forms.PictureBox();
			this.textBox = new System.Windows.Forms.TextBox();
			this.player = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.ground)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.malykaktus1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.malykaktus2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.malykaktus3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.duzykaktus1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.duzykaktus2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.duzykaktus3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.player)).BeginInit();
			this.SuspendLayout();
			// 
			// GameTimer
			// 
			this.GameTimer.Enabled = true;
			this.GameTimer.Interval = 20;
			this.GameTimer.Tick += new System.EventHandler(this.GameTimerTick);
			// 
			// ground
			// 
			this.ground.BackColor = System.Drawing.Color.White;
			this.ground.Image = ((System.Drawing.Image)(resources.GetObject("ground.Image")));
			this.ground.Location = new System.Drawing.Point(-10, 400);
			this.ground.Name = "ground";
			this.ground.Size = new System.Drawing.Size(1020, 50);
			this.ground.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.ground.TabIndex = 1;
			this.ground.TabStop = false;
			// 
			// malykaktus1
			// 
			this.malykaktus1.Image = ((System.Drawing.Image)(resources.GetObject("malykaktus1.Image")));
			this.malykaktus1.Location = new System.Drawing.Point(-129, 395);
			this.malykaktus1.Name = "malykaktus1";
			this.malykaktus1.Size = new System.Drawing.Size(34, 50);
			this.malykaktus1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.malykaktus1.TabIndex = 2;
			this.malykaktus1.TabStop = false;
			this.malykaktus1.Tag = "obstacle";
			// 
			// malykaktus2
			// 
			this.malykaktus2.Image = ((System.Drawing.Image)(resources.GetObject("malykaktus2.Image")));
			this.malykaktus2.Location = new System.Drawing.Point(-120, 395);
			this.malykaktus2.Name = "malykaktus2";
			this.malykaktus2.Size = new System.Drawing.Size(66, 50);
			this.malykaktus2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.malykaktus2.TabIndex = 3;
			this.malykaktus2.TabStop = false;
			this.malykaktus2.Tag = "obstacle";
			// 
			// malykaktus3
			// 
			this.malykaktus3.Image = ((System.Drawing.Image)(resources.GetObject("malykaktus3.Image")));
			this.malykaktus3.Location = new System.Drawing.Point(-120, 395);
			this.malykaktus3.Name = "malykaktus3";
			this.malykaktus3.Size = new System.Drawing.Size(101, 50);
			this.malykaktus3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.malykaktus3.TabIndex = 4;
			this.malykaktus3.TabStop = false;
			this.malykaktus3.Tag = "obstacle";
			// 
			// duzykaktus1
			// 
			this.duzykaktus1.BackColor = System.Drawing.Color.White;
			this.duzykaktus1.Image = ((System.Drawing.Image)(resources.GetObject("duzykaktus1.Image")));
			this.duzykaktus1.Location = new System.Drawing.Point(-120, 375);
			this.duzykaktus1.Name = "duzykaktus1";
			this.duzykaktus1.Size = new System.Drawing.Size(49, 70);
			this.duzykaktus1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.duzykaktus1.TabIndex = 5;
			this.duzykaktus1.TabStop = false;
			this.duzykaktus1.Tag = "obstacle";
			// 
			// duzykaktus2
			// 
			this.duzykaktus2.Image = ((System.Drawing.Image)(resources.GetObject("duzykaktus2.Image")));
			this.duzykaktus2.Location = new System.Drawing.Point(-120, 375);
			this.duzykaktus2.Name = "duzykaktus2";
			this.duzykaktus2.Size = new System.Drawing.Size(100, 70);
			this.duzykaktus2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.duzykaktus2.TabIndex = 6;
			this.duzykaktus2.TabStop = false;
			this.duzykaktus2.Tag = "obstacle";
			// 
			// duzykaktus3
			// 
			this.duzykaktus3.Image = ((System.Drawing.Image)(resources.GetObject("duzykaktus3.Image")));
			this.duzykaktus3.Location = new System.Drawing.Point(-120, 375);
			this.duzykaktus3.Name = "duzykaktus3";
			this.duzykaktus3.Size = new System.Drawing.Size(100, 70);
			this.duzykaktus3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.duzykaktus3.TabIndex = 7;
			this.duzykaktus3.TabStop = false;
			this.duzykaktus3.Tag = "obstacle";
			// 
			// textBox
			// 
			this.textBox.BackColor = System.Drawing.Color.White;
			this.textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox.Enabled = false;
			this.textBox.Font = new System.Drawing.Font("Arial", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.textBox.Location = new System.Drawing.Point(41, 22);
			this.textBox.Name = "textBox";
			this.textBox.Size = new System.Drawing.Size(487, 27);
			this.textBox.TabIndex = 9;
			this.textBox.Text = "Wynik: ";
			// 
			// player
			// 
			this.player.Image = ((System.Drawing.Image)(resources.GetObject("player.Image")));
			this.player.Location = new System.Drawing.Point(140, 375);
			this.player.Name = "player";
			this.player.Size = new System.Drawing.Size(51, 70);
			this.player.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.player.TabIndex = 10;
			this.player.TabStop = false;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(992, 566);
			this.Controls.Add(this.player);
			this.Controls.Add(this.textBox);
			this.Controls.Add(this.duzykaktus3);
			this.Controls.Add(this.duzykaktus2);
			this.Controls.Add(this.duzykaktus1);
			this.Controls.Add(this.malykaktus3);
			this.Controls.Add(this.malykaktus2);
			this.Controls.Add(this.malykaktus1);
			this.Controls.Add(this.ground);
			this.Name = "MainForm";
			this.Text = "T-rex runner";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFormKeyDown);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFormKeyUp);
			((System.ComponentModel.ISupportInitialize)(this.ground)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.malykaktus1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.malykaktus2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.malykaktus3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.duzykaktus1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.duzykaktus2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.duzykaktus3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.player)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.TextBox textBox;
		private System.Windows.Forms.PictureBox duzykaktus3;
		private System.Windows.Forms.PictureBox duzykaktus2;
		private System.Windows.Forms.PictureBox duzykaktus1;
		private System.Windows.Forms.PictureBox malykaktus3;
		private System.Windows.Forms.PictureBox malykaktus2;
		private System.Windows.Forms.PictureBox malykaktus1;
		private System.Windows.Forms.PictureBox ground;
		private System.Windows.Forms.PictureBox player;
		private System.Windows.Forms.Timer GameTimer;
	}
}
